# Creating custom ARM-based Linux via buildroot
In this small tutorial; i am trying to enumarate, how to compile custom ARM linux using [Buildroot](https://buildroot.org/). 

**IMPORTANT**: Please don't forget; these configurations are just for myself. You can add or exclude some packages for yourself.

I have created a Qemu folder under my home directory. 
```
$ mkdir Qemu
$ cd Qemu
```

Now download latest buildroot package from their [web site's download page](https://buildroot.org/download.html). 
```
$ wget https://buildroot.org/downloads/buildroot-{date}.tar.gz
```

Then uncompress the package:
```
$ tar -xvf buildroot-{date}.tar.gz
$ cd buildroot-{date}
```

## Select proper menuconfig
Under `buildroot-{date} > configs` directory find `qemu_arm_versatile_defconfig` for ARM LE 32bit configuration. Under buildroot-{date} directory run the command below:
```
$ make qemu_arm_versatile_defconfig
```

## To configure our custom linux build:
```
$ make xconfig
```
You can also use `menuconfig`, `nconfig` or `gconfig` instead of `xconfig`

### Target Packages
    Click "Target Packages" and select
        a. Show packages that are also provided by busybox
        b. Individual binaries
        
    Click "Build Options" and select
        a. Enable compiler cache(optional)
        b. build packages with debugging symbols(optional)
        
    Click "Toolchain" and select
        a. Enable WCHAR support
        b. Enable toolchain locale/i18n support
        c. Thread library debugging(optional)
        d. Enable C++ support
        e. Build cross gdb for the host
            + Python support
            + Simulator support(optional)
        f. Register toolchain within Eclipse Buildroot plug-in
        
        Note: I didn't change the default versions of gcc or binutils. These configuration is enough for me. But you can change them, if you need.
        
    Click "System configuration" and select
        a. System hostname -> change this(optional)
        b. System banner -> change this(optional)
        c. Enable root login with password and enter a password
        d. bin/sh -> change to zsh
        e. Install timezoneinfo
        
        Note: You don't need to change /bin/sh, this configuration is for me and i just want to use zsh =)
        
    Under "Target Packages > Compression" select
        1. bzip2
        2. gzip
        3. p7zip
        4. unrar
        5. unzip
        6. xz-utils
        7. zip
        We can make our compression/decompression process on host machine but to have these utils under the hood might come in handy
        
    Under "Target Packages > Debugging" select
        1. gdb
            a. gdbserver
            b. full debugger
        2. lsof
        3. memstat
        4. strace
        
    Under "Target Packages > Development Tools" select
        1. binutils
            a. binutils binary
        2. findutils
        3. grep
        4. git
        5. make
        6. sed
        7. tree

    Under "Target Packages > Interpretter" select
        1. perl
        2. Python3
            a. py and pyc
        3. External python modules
            a. python-requests
            
    Under "Target Packages > Library > Crypto" select
        1. CA certificates
        2. libssh
            a. libssh server
        3. libssh2
        4. openssl
            a. openssl binary
            b. openssl additional engines
        
    Under "Target Packages > Misc > collectd" select
        1. read plugin
            a. curl
            b. df
            c. ping
            
    Under "Target Packages > Networking" select
        1. openssh
        2. tcpdump
            a. smb dump support
        3. tftpd
        4. wget
        5. whois
        
    Under "Target Packages > Shell" select
        1. file
        2. screen
        3. sudo
        4. time
        5. which
        
    Under "Target Packages > System Tools" select
        1. htop
        2. tar
    
    Under "Target Packages > System Tools > util-linux" select
        1. cal
        2. hwlock
        3. kill
        4. rename
        5. su
        6. vipw
        
    Under "Target Packages > Text Edit" select
        1. less
        2. nano
        3. vim

    Under "Filesystem images" select
        1. exact size: ??M Expand this value if you select lots of packages to install.
        
Save and exit        

## To compile
Simply run the command below:
```
$ make
```
To compile with 4 processors run:
```
$ make -j4
```
        
## After compiling.
1. Move "output" folder where you want to work:
```
$ sudo mv output ../Qemu/CustomARM
```
Then change your directory where new filesystem and kernel resides:
```
$ cd ~/Qemu/CustomARM/images
```
    
2. To emulate the custom arm kernel on your host machine run this command:
```
$ qemu-system-arm -M versatilepb -kernel zImage -dtb versatile-pb.dtb -drive file=rootfs.ext2,if=scsi,format=raw -append "root=/dev/sda console=ttyAMA0,115200" -serial stdio -name VERSATILE_ARM_MAKINE -netdev user,id=ethernet.0,hostfwd=tcp::2222-:22 -device rtl8139,netdev=ethernet.0
```
To emulate without a display use `-nographic` parameter
```
qemu-system-arm -nographic -M versatilepb -kernel zImage -dtb versatile-pb.dtb -drive file=rootfs.ext2,if=scsi,format=raw -append "root=/dev/sda console=ttyAMA0,115200" -serial mon:stdio -name VERSATILE_ARM_MAKINE -netdev user,id=ethernet.0,hostfwd=tcp::2222-:22 -device rtl8139,netdev=ethernet.0
```

* Explanations of commandline arguments:
```
qemu-system-arm : emulates an arm little endian system
-nographic  : disable graphical output and redirect serial I/Os to console
-M          : selects the emulated machine
-kernel     : path to which kernel image to use
-dtb        : use file as a device tree binary (dtb) image and pass it to the kernel on boot.
-serial     : redirects the machine's virtual serial port to our host's stdio
-net        : this configures the machine's network stack to attach a NIC
-name       : assigns symbolic name for use in monitor commands.
-device     : adds device driver
-append     : uses the following quote for kernel commandline. Here we can provide the boot args direct to the kernel, 
              telling it where to find it's root filesytem and what type it is
-drive      : defines a new drive. file defines which disk image to use with this drive. 
              format option specifies which disk format will be used rather than detecting the format.
              Can be used to specifiy format=raw to avoid interpreting an untrusted format header.
              if=interface, this option defines on which type on interface the drive is connected. 
              Available types are: ide, scsi, sd, mtd, floppy, pflash, virtio.
              
hostfwd=[tcp|udp]:[hostaddr]:hostport-[guestaddr]:guestport Redirect incoming TCP or UDP connections 
to the host port hostport to the guest IP address guestaddr on guest port guestport.

optional:
-no-reboot : exits qemu-system instead of rebooting
```

3. When you see login prompt log in with your root credentials then run ssh-keygen command and create key pair for your root account
```
$ ssh-keygen
```

4. Create another user and a home directory for that user.
```
$ adduser cracker
$ mkdir /home
$ cd /home
$ mkdir cracker
$ chown -R cracker:cracker cracker/
```
5. Set clock:
```
date -s "2020-04-02 10:52"
```
6. [OPTIONAL] Install oh-my-zsh
```
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh --no-check-certificate)"
```

7. Run "exit" command and see login prompt again. Then log in with new user. Run ssh-keygen for this new user (in its home directory /home/cracker). If you want oh-my-zsh in you user, then install it again. We are ready for ssh and scp

8. Connecting from host to ARM machine through SSH
```
ssh cracker@localhost -p 2222
ssh root@localhost -p 2222
```
If you fail login in with ssh read the "important notes" section below

9. Sending files from host to qemu arm machine through SCP:
```
scp -P2222 armbinary cracker@localhost:/home/cracker
scp -P2222 armbinary root@localhost:/root/
```

10. To execute arm binary from host run the command below:
```
$ qemu-arm -L ../Qemu/BuildRoot/buildroot-{date}/output/host/arm-buildroot-linux-uclibcgnueabi/sysroot armbinary 
or
$ qemu-arm -L Qemu/CustomARM/host/arm-buildroot-linux-uclibcgnueabi/sysroot Downloads/Workshop/armbinary
```

* Explanations of commandline arguments
```
qemu-arm    : The qemu-user emulator can run binaries for other architectures but with the same operating system as the current one.
-L <path>   : Set the elf interpreter prefix (default=/etc/qemu-binfmt/%M).
-g          : Wait gdb connection to port 1234.
```

11. To debug the arm binary from host machine you should install:

* [gdb-multiarch](http://www.gnu.org/software/gdb/)

    * Debian-based
    ```
    $ sudo apt install gdb-multiarch
    ```
    
    * Arch-linux
    ```
    $ yay -S gdb-multiarch
    ```

* [gef - GDB Enhanced Features for exploit devs & reversers](https://github.com/hugsy/gef)
```
$ wget -q -O- https://github.com/hugsy/gef/raw/master/scripts/gef.sh | sh
```

12. How to debug
* Open one terminal and navigate through our target binary -it is in Downloads/Workout directory in my situation-, then enter the command below:
```
$ qemu-arm -L ../Qemu/BuildRoot/buildroot-{date}/QemuArm-uClibc/host/arm-buildroot-linux-uclibcgnueabi/sysroot -g 12345 armbinary 
```

* Open another terminal and navigate through our target binary then call the command below:
```
$ gdb-multiarch armbinary
```
Wait till `gef>` and blinking cursor shows up. Then enter these parameters:
```
gef>  set arch arm
gef>  set endian little
gef>  target remote localhost:12345
```
Write `quit` to exit gef+gdb.

13. Important Notes
* Sometimes we can't ssh or scp over root or we get password errors even ve enter the right password.

Edit sshd_config file:
```
vim /etc/ssh/sshd_config
```
Change the first line like the second then save sshd_config file:
```
#PermitRootLogin prohibited-password    ->change this line to
PermitRootLogin yes                     ->this
```

* Sometimes we need to manually add our host systems id_rsa.pub key to arm machine. So in root home directory :
```
$ cd .ssh/
$ vim authorized_keys #enter your hosts key id_rsa.pub file here, then save. Reboot.
```

* In some situations we need to manually edit /etc/shadow file
```
# vim /etc/shadow
root:$1$RMIhYpr$x1OkHx7V/gUWuGSHoCQiW0:10933:0:99999:7:::
cracker:$1$It5lw8P$qPkJH8bkHjRbR3nSSnZD9/:10933:0:99999:7:::
```
Look at the number `10933`; if it is different, then change it to `10933`!

* Sometimes we need to edit /etc/pam.d/su file. Beacuse our user can't su or sudo:
```
auth		sufficient	pam_rootok.so
#auth		required	pam_wheel.so use_uid -----> comment this out like here
auth		required	pam_env.so
auth		required	pam_unix.so nullok

account		required	pam_unix.so

password	required	pam_unix.so nullok

# session	required	pam_selinux.so close
session		required	pam_limits.so
session		required	pam_env.so
session		required	pam_unix.so
session		optional	pam_lastlog.so
# session	required	pam_selinux.so open
```

## Deployment

I have tried these commands or notes on both kali linux and arch linux. Most probably these can run on your favourite linux distrubition.

## Authors

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)
* **ErrorInside** - *Reverser* - [errorinside](http://gitlab.com/errorinside)

**HomePage** - [SCTZine](http://www.sctzine.com)

## License

This project is under the MIT License
