# Creating custom PPC-based Linux via buildroot
In this small tutorial; i am trying to enumarate, how to compile custom PPC linux using [Buildroot](https://buildroot.org/). 

**IMPORTANT**: Please don't forget; these configurations are just for myself. You can add or exclude some packages for yourself.

I have created a Qemu folder under my home directory. 
```
$ mkdir Qemu
$ cd Qemu
```

Now download latest buildroot package from their [web site's download page](https://buildroot.org/download.html). 
```
$ wget https://buildroot.org/downloads/buildroot-{date}.tar.gz
```

Then uncompress the package:
```
$ tar -xvf buildroot-{date}.tar.gz
$ cd buildroot-{date}
```

## Select proper menuconfig
Under `buildroot-{date} > configs` directory find `qemu_ppc_g3beige_defconfig` for PPC 32bit configuration(This machine is Big Endian by default). Under buildroot-{date} directory run the command below:
```
$ make qemu_ppc_g3beige_defconfig
```

## To configure our custom linux build:
```
$ make xconfig
```
You can also use `menuconfig`, `nconfig` or `gconfig` instead of `xconfig`

### Target Packages
    Click "Target Packages" and select
        a. Show packages that are also provided by busybox
        b. Individual binaries
        
    Click "Build Options" and select
        a. Enable compiler cache(optional)
        b. build packages with debugging symbols(optional)
        
    Click "Toolchain" and select
        a. Enable WCHAR support
        b. Enable toolchain locale/i18n support
        c. Thread library debugging(optional)
        d. Enable C++ support
        e. Build cross gdb for the host
            + Python support
            + Simulator support(optional)
        f. Register toolchain within Eclipse Buildroot plug-in
        
        Note: I didn't change the default versions of gcc or binutils. These configuration is enough for me. But you can change them, if you need.
        
    Click "System configuration" and select
        a. System hostname -> change this(optional)
        b. System banner -> change this(optional)
        c. Enable root login with password and enter a password
        d. bin/sh -> change to zsh
        e. Install timezoneinfo
        
        Note: You don't need to change /bin/sh, this configuration is for me and i just want to use zsh =)
        
    Under "Target Packages > Compression" select
        1. bzip2
        2. gzip
        3. p7zip
        4. unrar
        5. unzip
        6. xz-utils
        7. zip
        We can make our compression/decompression process on host machine but to have these utils under the hood might come in handy
        
    Under "Target Packages > Debugging" select
        1. gdb
            a. gdbserver
            b. full debugger
        2. lsof
        3. strace
        
    Under "Target Packages > Development Tools" select
        1. binutils
            a. binutils binary
        2. findutils
        3. grep
        4. git
        
    Under "Target Packages > Interpretter" select
        1. Python
            a. py and pyc
            
    Under "Target Packages > Library > Crypto" select
        1. CA certificates
        2. libssh
        3. libssh2
        4. openssl
        
    Under "Target Packages > Misc > collectd" select
        1. read plugin
            a. curl
            b. df
            
    Under "Target Packages > Networking" select
        1. openssh
        2. tftpd
        3. wget
        4. whois
        
    Under "Target Packages > Shell" select
        1. file
        2. screen
        3. sudo
        4. which
        
    Under "Target Packages > System Tools" select
        1. htop
        2. tar
    
    Under "Target Packages > System Tools > util-linux" select
        1. kill
        2. su
        3. vipw
        4.powerpc-utils
        
    Under "Target Packages > Text Edit" select
        1. less
        2. nano
        3. vim

    Under "Filesystem images" select
        1. exact size: ??M Expand this value if you select lots of packages to install.
        
Save and exit        

## To compile
Simply run the command below:
```
$ make
```
To compile with 4 processors run:
```
$ make -j4
```
        
## After compiling.
1. Move "output" folder where you want to work:
```
$ sudo mv output ../Qemu/CustomPPC
```
Then change your directory where new filesystem and kernel resides:
```
$ cd ~/Qemu/CustomPPC/images
```
    
2. To emulate the custom ppc kernel on your host machine run this command:
```
$ qemu-system-ppc -M g3beige -kernel vmlinux -drive file=rootfs.ext2,format=raw -append "console=ttyS0 root=/dev/hda" -serial stdio -name PPC32BE_EXT2_SCT -netdev user,id=ethernet.0,hostfwd=tcp::3333-:22 -device rtl8139,netdev=ethernet.0
```

* Explanations of commandline arguments:
```
qemu-system-ppc : emulates a ppc big endian system
-M          : selects the emulated machine
-kernel     : path to which kernel image to use
-serial     : redirects the machine's virtual serial port to our host's stdio
-net        : this configures the machine's network stack to attach a NIC
-name       : assigns symbolic name for use in monitor commands.
-device     : adds device driver
-append     : uses the following quote for kernel commandline. Here we can provide the boot args direct to the kernel, 
              telling it where to find it's root filesytem and what type it is
-drive      : defines a new drive. file defines which disk image to use with this drive. 
              format option specifies which disk format will be used rather than detecting the format.
              Can be used to specifiy format=raw to avoid interpreting an untrusted format header.
              if=interface, this option defines on which type on interface the drive is connected. 
              Available types are: ide, scsi, sd, mtd, floppy, pflash, virtio.
              
hostfwd=[tcp|udp]:[hostaddr]:hostport-[guestaddr]:guestport Redirect incoming TCP or UDP connections 
to the host port hostport to the guest IP address guestaddr on guest port guestport.

optional:
-no-reboot : exits qemu-system instead of rebooting
```

3. When you see login prompt log in with your root credentials then run ssh-keygen command and create key pair for your root account
```
$ ssh-keygen
```

4. Create another user and a home directory for that user.
```
$ adduser cracker
$ mkdir /home
$ cd /home
$ mkdir cracker
$ chown -R cracker:cracker cracker/
```

5. Run "exit" command and see login prompt again. Then log in with new user. Run ssh-keygen for this new user (in its home directory /home/cracker). We are ready for ssh and scp

6. Connecting from host to PPC machine through SSH
```
ssh cracker@localhost -p 3333
ssh root@localhost -p 3333
```
If you fail login in with ssh read the "important notes" section below

7. Sending files from host to qemu ppc machine through SCP:
```
scp -P3333 ppcbinary cracker@localhost:/home/cracker
scp -P3333 ppcbinary root@localhost:/root/
```

8. To execute ppc binary from host run the command below:
```
$ qemu-ppc -L ../Qemu/BuildRoot/buildroot-{date}/output/host/powerpc-buildroot-linux-uclibc/sysroot ppcbinary 12123123
or
$ qemu-ppc -L Qemu/CustomPPC/host/powerpc-buildroot-linux-uclibc/sysroot Downloads/Workshop/ppcbinary
```

* Explanations of commandline arguments
```
qemu-ppc    : The qemu-user emulator can run binaries for other architectures but with the same operating system as the current one.
-L <path>   : Set the elf interpreter prefix (default=/etc/qemu-binfmt/%M).
-g          : Wait gdb connection to port 1234.
```

9. To debug the ppc binary from host machine you should install:

* [gdb-multiarch](http://www.gnu.org/software/gdb/)

    * Debian-based
    ```
    $ sudo apt install gdb-multiarch
    ```

    * Arch-linux
    ```
    $ yaourt -S gdb-multiarch
    ```

* [gef - GDB Enhanced Features for exploit devs & reversers](https://github.com/hugsy/gef)
```
$ wget -q -O- https://github.com/hugsy/gef/raw/master/scripts/gef.sh | sh
```

10. How to debug
* Open one terminal and navigate through our target binary -it is in Downloads/Workout directory in my situation-, then enter the command below:
```
$ qemu-ppc -L Qemu/CustomPPC/host/powerpc-buildroot-linux-uclibc/sysroot -g 12345 Downloads/Workshop/ppcbinary
```

* Open another terminal and navigate through our target binary then call the command below:
```
$ gdb-multiarch ppcbinary
```
Wait till `gef>` and blinking cursor shows up. Then enter these parameters:
```
gef>  set arch powerpc:750
gef>  set endian big
gef>  target remote localhost:12345
```
Write `quit` to exit gef+gdb.

11. Important Notes
* Sometimes we can't ssh or scp over root or we get password errors even ve enter the right password.

Edit sshd_config file:
```
vim /etc/ssh/sshd_config
```
Change the first line like the second then save sshd_config file:
```
#PermitRootLogin prohibited-password    ->change this line to
PermitRootLogin yes                     ->this
```

* Sometimes we need to manually add our host systems id_rsa.pub key to ppc machine. So in root home directory :
```
$ cd .ssh/
$ vim authorized_keys #enter your hosts key id_rsa.pub file here, then save. Reboot.
```

* In some situations we need to manually edit /etc/shadow file
```
# vim /etc/shadow
root:$1$RMIhYpr$x1OkHx7V/gUWuGSHoCQiW0:10933:0:99999:7:::
cracker:$1$It5lw8P$qPkJH8bkHjRbR3nSSnZD9/:10933:0:99999:7:::
```
Look at the number `10933`; if it is different, then change it to `10933`!

## Deployment

I have tried these commands or notes on both kali linux and arch linux. Most probably these can run on your favourite linux distrubition.

## Authors

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)
* **ErrorInside** - *Reverser* - [errorinside](http://gitlab.com/errorinside)

**HomePage** - [SCTZine](http://www.sctzine.com)

## License

This project is under the MIT License
